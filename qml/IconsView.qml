import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Terminalaccess 1.0
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Components.Popups 0.1
import "components"

Page {
    id: iconsViewPage

        header: state == "default" ? defaultHeader : searchPageHeader
        state: "default"
        
        PageHeader {
            id: defaultHeader                
            title: "Custom phablet tools"
                

        }
        

    property string iconFolderName:"";
    property string iconName:"";
    property string iconType:"";
    property string iconAuthors:"";
    property string iconUpdate:"";
    property string iconCategories:"";
    property string iconLicense:"";
    property string iconWebsite:"";
    
    Rectangle {
        id:main
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            top: iconsViewPage.header.bottom
        }
        color: "#111111"

        Flickable {
            id: flickable
            anchors.fill: parent
            contentHeight: iconsViewColumn.height
            flickableDirection: Flickable.VerticalFlick
            clip: true


            Column {
                id: iconsViewColumn
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }

                Rectangle {
                    id: headerpicture
                    width: parent.width;
                    height: units.gu(26)
                    color: "#111111"

                    Image {
                        id : picturebackgroundtop;
                        source:"https://framagit.org/ubuntouch-fr-dev/custom-phablet-tools/raw/media/icons/"+iconFolderName+"/prez.png";
                        width: parent.width;
                        height: units.gu(25)
                        fillMode: Image.PreserveAspectCrop

                    }

                    Image {
                        id : productImage;
                        fillMode: Image.PreserveAspectCrop
                        visible: false // Do not forget to make original pic insisible
                    }

                    
                    Rectangle {
                        id: mask
                        anchors.right: parent.right; anchors.verticalCenter: picturebackgroundtop.bottom; anchors.rightMargin: 7;
                        width: units.gu(7)
                        height: units.gu(7)
                        color: "#111111"
                        radius: 120
                        clip: true
                        visible: true

                        Rectangle {
                            id: mask2
                            anchors.horizontalCenter: parent.horizontalCenter; anchors.verticalCenter: mask.verticalCenter;
                            width: units.gu(6)
                            height: units.gu(6)
                            radius: 120
                            color: "#3B3B3B"
                            clip: true
                            visible: true

                            Icon {
                                id: chooseIcon
                                name: "save"
                                anchors.horizontalCenter: parent.horizontalCenter; anchors.verticalCenter: parent.verticalCenter;
                                width: units.gu(3)
                                height: units.gu(3)
                                color: "#3EB34F"
                            }
                        }

                       Component {
                            id: waitDialog
                            Dialog {
                                id: waitDialogue

                                text: i18n.tr("Wait please")
                                    
                                ProgressBar {
                                    indeterminate: true
                                }

                            }
                        }                        
                        
                       Component {
                            id: confirmDialog
                            Dialog {
                                id: confirmDialogue

                                title: i18n.tr("Apply Icons")
                                text: i18n.tr("Do you want to apply the icons now?") +
                                    "<br />" + i18n.tr("All currently open apps will be closed.") +
                                    "<br />" + i18n.tr("Save all your work before continuing!")

                                Button{
                                    text: i18n.tr("Cancel")
                                    onClicked: PopupUtils.close(confirmDialogue);
                                }

                                Button{
                                    text: i18n.tr("Apply icons")
                                    color: theme.palette.normal.negative
                                    onClicked: {
                                        Terminalaccess.run("sudo -S bash assets/updateicon.sh "+iconFolderName+" && restart unity8")
                                        onClicked: PopupUtils.close(confirmDialogue);
                                        PopupUtils.open(waitDialog);
                                    }
                                }
                            }
                        }
                        MouseArea {
                            anchors.fill: mask
                            onClicked: {
                                PopupUtils.open(confirmDialog);
                            }
                        }
                    }
                    
                    Rectangle {
                        id: installmask
                        anchors.right: parent.right; anchors.top: mask.bottom; anchors.rightMargin: 7;
                        width: parent.width/2
                        height: units.gu(3)
                        color: "#111111"
                        radius: 120
                        clip: true
                        visible: true
                            
                            Text {
                                anchors.right: parent.right; anchors.verticalCenter: parent.verticalCenter;     anchors.rightMargin: 7;
                                text: i18n.tr("Install")
                                color: "#ffffff"
                            }
                    }
                    
                   Rectangle {
                        id: rmask
                        anchors.left: parent.left; anchors.verticalCenter: picturebackgroundtop.bottom; anchors.leftMargin: 7;
                        width: units.gu(7)
                        height: units.gu(7)
                        color: "#111111"
                        radius: 120
                        clip: true
                        visible: true

                        Rectangle {
                            id: rmask2
                            anchors.horizontalCenter: parent.horizontalCenter; anchors.verticalCenter: rmask.verticalCenter;
                            width: units.gu(6)
                            height: units.gu(6)
                            radius: 120
                            color: "#3B3B3B"
                            clip: true
                            visible: true

                            Icon {
                                id: rchooseIcon
                                name: "reset"
                                anchors.horizontalCenter: parent.horizontalCenter; anchors.verticalCenter: parent.verticalCenter;
                                width: units.gu(3)
                                height: units.gu(3)
                                color: "#ED3146"
                            }
                        }
                       Component {
                            id: confirmDialogR
                            Dialog {
                                id: confirmDialogueR

                                title: i18n.tr("Restore Icons")
                                text: i18n.tr("Do you want to restore the icons now?") +
                                    "<br />" + i18n.tr("All currently open apps will be closed.") +
                                    "<br />" + i18n.tr("Save all your work before continuing!")

                                Button{
                                    text: i18n.tr("Cancel")
                                    onClicked: PopupUtils.close(confirmDialogueR);
                                }

                                Button{
                                    text: i18n.tr("Restore icons")
                                    color: theme.palette.normal.negative
                                    onClicked: {
                                        Terminalaccess.run("sudo -S bash assets/restoreicon.sh ; restart unity8")
                                        onClicked: PopupUtils.close(confirmDialogueR);
                                        PopupUtils.open(waitDialog);
                                    }
                                }
                            }
                        }
                       
                        MouseArea {
                            anchors.fill: rmask
                            onClicked: {
                                  PopupUtils.open(confirmDialogR);
                            }
                        }
                       
                    }
                    
                    Rectangle {
                        id: restoremask
                        anchors.left: parent.left; anchors.top: rmask.bottom; anchors.leftMargin: 7;
                        width: parent.width/2
                        height: units.gu(3)
                        color: "#111111"
                        radius: 120
                        clip: true
                        visible: true
                            
                            Text {
                                anchors.left: parent.left; anchors.verticalCenter: parent.verticalCenter;
                                anchors.leftMargin: 7;
                                text: i18n.tr("Restore")
                                color: "#ffffff"
                            }
                    }
                    
                    
                } // header picture


                Column {
                    id: detailsItem
                    width: parent.width-units.gu(4)
                    anchors.horizontalCenter: parent.horizontalCenter
                                
                Text {
                    text: iconName
                    color: "#FFFFFF"
                    font.pointSize: units.gu(2) 
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                
                Text {
                    text: i18n.tr("by")+" "+iconAuthors+"<br /><br />"
                    color: "#AEA79F"
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                
                ListItem.Header {
                    text: "<font color=\"#ffffff\">"+i18n.tr("Item details")+"</font>"
                }     
                
                Text {
                    text: "<font color=\"#ffffff\">"+i18n.tr("Last update")+": </font>"+Date.fromLocaleString(Qt.locale(), ""+iconUpdate+"", "yyyyMMdd").toLocaleDateString(Qt.locale(), Locale.ShortFormat)+
                            "<br /><font color=\"#ffffff\">"+i18n.tr("categories")+": </font>"+iconCategories+
                        "<br /><font color=\"#ffffff\">"+i18n.tr("License")+": </font>"+iconLicense+
                        "<br /><font color=\"#ffffff\">"+i18n.tr("Website")+": </font>"+iconWebsite
                    color: "#AEA79F"
                }
            }                
                
            } //Column
        } //flickable
    } //rectangle
} //page

import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Terminalaccess 1.0
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.Components.Popups 0.1
import "components"
    
Page {
    id: iconsPage

        header: state == "default" ? defaultHeader : searchPageHeader
        state: "default"
        
        PageHeader {
            id: defaultHeader
            visible: iconsPage.state == "default"
                
            title: "Custom phablet tools"
                
            leadingActionBar.actions: [
                Action {
                    iconName: "home"
                    text: i18n.tr("Home")
                    onTriggered: {
                        pageStack.pop();
                    }
                }, 
                Action {
                    iconName: "import-image"
                    text: i18n.tr("Wallpapers")
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("Wallpaper.qml"));
                    }
                },
                Action {
                    iconName: "keypad"
                    text: i18n.tr("Icons")
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("Icons.qml"));
                    }
                }
            ]
            trailingActionBar {
                actions: [
                    Action {
                        iconName: "search"
                        text: i18n.tr("search")
                        onTriggered: {
                            iconsPage.state = "search"
                        }
                    }//,
                    //Action {
                    //    iconName: "filter"
                    //    text: "filter"
                    //}
               ]
               numberOfSlots: 2
            }
        }
        
    PageHeader {
        id: searchPageHeader
        visible: iconsPage.state == "search"
        title: i18n.tr("Search")
        leadingActionBar {
            actions: [
                Action {
                    id: closePageAction
                    text: i18n.tr("Close")
                    iconName: "back"
                    onTriggered: {
                        iconsPage.state = "default"
                        jsonicons.query = "$[*]"

                    }
                }

            ]
        }/*
            trailingActionBar {
                actions: [
                    Action {
                        iconName: "filter"
                        text: "filter"
                    }
               ]
               numberOfSlots: 1
            }*/

        contents: Rectangle {
            color: "#fff"
            anchors {
                left: parent.left
                right: parent.right
                verticalCenter: parent.verticalCenter
            }
            TextField {
                id: searchField
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                }
                primaryItem: Icon {
                    anchors.leftMargin: units.gu(0.2)
                    height: parent.height*0.5
                    width: height
                    name: "find"
                }
                hasClearButton: true
                inputMethodHints: Qt.ImhNoPredictiveText
                onVisibleChanged: {
                    if (visible) {
                        forceActiveFocus()
                    }
                }
                onTextChanged: {
                    if (searchField.text == ""){
                        jsonicons.query = "$[*]"
                    }else{
                        jsonicons.query = "$[?(/"+searchField.text+"/i.test(@.name))]"
                    }
                }
            }
        }
    }
    
    Rectangle {
        id:main
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            top: iconsPage.header.bottom
        }
        color: "#111111"
            
            
    ActivityIndicator {
        id: activity
        running: true
        anchors.centerIn: parent
    }
        
        Flickable {
            id: flickable
            anchors.fill: parent
            contentHeight: iconsColumn.height
            flickableDirection: Flickable.VerticalFlick
            clip: true

    SortFilterModel {
        id: sortedjsonicons
        model: jsonicons.model
        sort.property: "name"
        sort.order: Qt.AscendingOrder
        sortCaseSensitivity: Qt.CaseInsensitive
    }
 
        ListView {
            anchors.fill: parent
                
            JSONListModel {
                id: jsonicons
                source: "https://framagit.org/ubuntouch-fr-dev/custom-phablet-tools/raw/media/icons/pack.json"
                query: "$[*]"
            }
            model: sortedjsonicons
 
                
            delegate: ListItem.SingleValue {
                showDivider: false
                progression: true
                iconSource: Qt.resolvedUrl("https://framagit.org/ubuntouch-fr-dev/custom-phablet-tools/raw/media/icons/"+model.folder+"/logo.png")
                text: model.name
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("IconsView.qml"), {"iconFolderName": model.folder, "iconName": model.name, "iconType": model.type, "iconAuthors": model.author, "iconUpdate": model.update, "iconCategories": "", "iconLicense": model.license, "iconWebsite": model.website});
                }
                
                        Component.onCompleted: {
                            activity.running = false
                        }
            }
            
            
        }                  


        } //flickable
    } //rectangle


        
} //page

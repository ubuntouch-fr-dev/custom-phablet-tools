import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Terminalaccess 1.0
import "components"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'custom-phablet-tools.kazord'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)
    
    theme.name: "Ubuntu.Components.Themes.SuruDark"
    
Component {
	id: diag
 Dialog {
	id: popup
	title: "Authentification needed"
	TextField {
		id:inp
		placeholderText: "Enter password (by defaut : phablet)"
		echoMode: TextInput.Password
	}
	Button {
		text:"ok"
		onClicked: {Terminalaccess.inputLine(inp.text, false);PopupUtils.close(popup)}
	}
}
}
    
PageStack {
        anchors.fill: parent
  id: pageStack
      
        Component.onCompleted: push(homePage);
      
    Page {
        id: homePage
        anchors.fill: parent

        header: state == "default" ? defaultHeader : searchPageHeader
        state: "default"
        
        PageHeader {
            id: defaultHeader
            visible: homePage.state == "default"
                
            title: "Custom phablet tools"
                
            leadingActionBar.actions: [ 
                Action {
                    iconName: "import-image"
                    text: i18n.tr("Wallpapers")
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("Wallpaper.qml"));
                    }
                },
                Action {
                    iconName: "keypad"
                    text: i18n.tr("Icons")
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("Icons.qml"));
                    }
                }/*,
                Action {
                    iconName: "new-message"
                    text: "Notification"
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("Notifications.qml"));
                    }
                },
                Action {
                    iconName: "speaker"
                    text: "ringtones"
                }*/
            ]
             trailingActionBar {
                actions: [
                    Action {
                        iconName: "info"
                        text: "about"
                        onTriggered: {
                             pageStack.push(Qt.resolvedUrl("About.qml"));
                        }
                    }/*,
                    Action {
                        iconName: "search"
                        text: "search"
                        onTriggered: {
                            homePage.state = "search"
                        }
                    }*/
               ]
               numberOfSlots: 2
            } 
        }
        
    PageHeader {
        id: searchPageHeader
        visible: homePage.state == "search"
        title: i18n.tr("Search")
        leadingActionBar {
            actions: [
                Action {
                    id: closePageAction
                    text: i18n.tr("Close")
                    iconName: "back"
                    onTriggered: {
                        homePage.state = "default"
                    }
                }

            ]
        }
            trailingActionBar {
                actions: [
                    Action {
                        iconName: "filter"
                        text: "filter"
                    }
               ]
               numberOfSlots: 1
            }

        contents: Rectangle {
            color: "#fff"
            anchors {
                left: parent.left
                right: parent.right
                verticalCenter: parent.verticalCenter
            }
            TextField {
                id: searchField
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                }
                primaryItem: Icon {
                    anchors.leftMargin: units.gu(0.2)
                    height: parent.height*0.5
                    width: height
                    name: "find"
                }
                hasClearButton: true
                inputMethodHints: Qt.ImhNoPredictiveText
                onVisibleChanged: {
                    if (visible) {
                        forceActiveFocus()
                    }
                }
                onAccepted: {

                }
            }
        }
    }


   
    Rectangle {
        id:main
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            top: homePage.header.bottom
        }
        color: "#111111"

        Flickable {
            id: flickable
            anchors.fill: parent
            flickableDirection: Flickable.VerticalFlick
            clip: true

    ActivityIndicator {
        id: activity
        running: true
        anchors.centerIn: parent
    }
                
SwipeView {
    id: view

    currentIndex: 0
    anchors { left: parent.left; right: parent.right; }
    height: units.gu(24)
            
    Item {
        id: firstPage
        Image {
            id: firstcarrousel
            width: parent.width;
            height: view.height
            fillMode: Image.PreserveAspectCrop
        }
        
                        Rectangle {
                            width: parent.width;
                            height: titlecaroussel1.contentHeight+units.gu(2)
                            color: "#000000"
                            anchors.bottom: firstcarrousel.bottom
                            opacity: 0.8
                            
                            Text{
                                id: titlecaroussel1
                                anchors.verticalCenter: parent.verticalCenter                                
                                horizontalAlignment: Text.AlignHCenter 
                                width: parent.width;
                                wrapMode:Text.WordWrap
                                color: "#ffffff"
                            }
                        }        
        
    }
    Item {
        id: secondPage
        Image {
            id: secondcarrousel
            width: parent.width;
            height: view.height
            fillMode: Image.PreserveAspectCrop
        }
                        Rectangle {
                            width: parent.width;
                            height: titlecaroussel2.contentHeight+units.gu(2)
                            color: "#000000"
                            anchors.bottom: secondcarrousel.bottom
                            opacity: 0.8
                            
                            Text{
                                id: titlecaroussel2
                                anchors.verticalCenter: parent.verticalCenter                                
                                horizontalAlignment: Text.AlignHCenter 
                                width: parent.width;
                                wrapMode:Text.WordWrap
                                color: "#ffffff"
                            }
                        }      
        
    }
}

PageIndicator {
    id: indicator

    count: view.count
    currentIndex: view.currentIndex

    anchors.top: view.top
    anchors.horizontalCenter: parent.horizontalCenter
}                
                
                
            ListItem.Header {
                anchors.top: view.bottom
                id: headerNewIcon
                text: "<font color=\"#ffffff\">"+i18n.tr("New pack of icons")+"</font>"
            }
            


    ListView {
        id: iconsView
        model: jsonicons.model
        anchors { left: parent.left; right: parent.right; top: headerNewIcon.bottom; }
        leftMargin: units.gu(2)
        rightMargin: units.gu(2)
        clip: true
        height: units.gu(10)
        visible: count > 0
        spacing: units.gu(1)
        orientation: ListView.Horizontal
        delegate: UbuntuShape {
            anchors.verticalCenter: parent.verticalCenter
            height: units.gu(8)
            width: units.gu(8)
            aspect: UbuntuShape.Flat
            sourceFillMode: UbuntuShape.PreserveAspectCrop
            source: Image {
                id: screenshotIcons
                    Component.onCompleted: if(index == 0) {
                        titlecaroussel1.text = i18n.tr("Icons")+": "+model.name
                        firstcarrousel.source = Qt.resolvedUrl("https://framagit.org/ubuntouch-fr-dev/custom-phablet-tools/raw/media/icons/"+model.folder+"/prez.png");
                    }
                source: Qt.resolvedUrl("https://framagit.org/ubuntouch-fr-dev/custom-phablet-tools/raw/media/icons/"+model.folder+"/logo.png")
                smooth: true
                antialiasing: true
            }

            AbstractButton {
                id: iconsButton
                anchors.fill: parent
                onClicked: pageStack.push(Qt.resolvedUrl("IconsView.qml"), {"iconFolderName": model.folder, "iconName": model.name, "iconType": model.type, "iconAuthors": model.author, "iconUpdate": model.update, "iconCategories": "", "iconLicense": model.license, "iconWebsite": model.website});
            }
        }
        
                        Component.onCompleted: {
                            activity.running = false
                        }
    }

            JSONListModel {
                id: jsonicons
                source: "https://framagit.org/ubuntouch-fr-dev/custom-phablet-tools/raw/media/icons/pack.json"
                query: "$[:10]"
            }
            
            
            ListItem.Header {
                id: headerNewWallpaper
                text: "<font color=\"#ffffff\">"+i18n.tr("New wallpapers")+"</font>"
                anchors { top: iconsView.bottom; }
            }
            


    ListView {
        id: wallpaperView
        model: jsonwallpaper.model
        anchors { left: parent.left; right: parent.right; top: headerNewWallpaper.bottom; }
        leftMargin: units.gu(2)
        rightMargin: units.gu(2)
        clip: true
        height: units.gu(16)
        visible: count > 0
        spacing: units.gu(1)
        orientation: ListView.Horizontal
        delegate: UbuntuShape {
            anchors.verticalCenter: parent.verticalCenter
            height:units.gu(14)
            width: units.gu(10)
            sourceFillMode: UbuntuShape.PreserveAspectCrop
            source: Image {
                id: screenshotWallpaper
                    Component.onCompleted: if(index == 0) {
                        titlecaroussel2.text = i18n.tr("Wallpaper")+": "+model.category
                        secondcarrousel.source = Qt.resolvedUrl(model.url_thumb);
                    }
                source: Qt.resolvedUrl(model.url_thumb)
                smooth: true
                antialiasing: true
            }

            AbstractButton {
                id: wallpaperButton
                anchors.fill: parent
                onClicked: pageStack.push(Qt.resolvedUrl("WallpaperView.qml"), {"idWallpaper": model.id});
            }
        }
        

    }            
            JSONListModel {
                id: jsonwallpaper
                source: "https://wall.alphacoders.com/api2.0/get.php?auth=2795f44dad7746122baaa83d01db8541&method=newest&page=1&info_level=2"
                query: "$..[:10]"
            }      
            

        } //flickable
    } //rectangle        
        
        
Connections {
	target: Terminalaccess
	//onNewOutputLineAvailable: {textoutput.append(Terminalaccess.getNewOutput())}
	//onNewErrorLineAvailable: {textoutput.append(Terminalaccess.getNewError())}
	onNeedSudoPassword: {PopupUtils.open(diag)}
	//onFinished: {Terminalaccess.run("sudo -S ls /")}
}
	}
/*
    Component.onCompleted: {
	Terminalaccess.run("sudo -S whoami")
	//returntext.text = Terminalaccess.outputUntilEnd()
	}
*/        
}
}
